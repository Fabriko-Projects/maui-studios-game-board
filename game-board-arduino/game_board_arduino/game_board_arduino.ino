
#define SERIAL false
#define SOUNDS_COUNT 10

struct Sound {
  byte    square;
  byte    out;
  byte    in;
  byte    len;
  boolean previousInState;  // HIGH or LOW, 
  boolean newTrigger;       // True if previousInState is LOW and current in state is HIGH (Just triggered it now)    
};

Sound sounds[SOUNDS_COUNT] {
  Sound {  1,  0, A1,  6 },  // 1
  Sound {  3,  1, A3, 11 },  // 2
  Sound {  6,  2, A4, 11 },  // 3
  Sound { 10,  3, A0, 11 },  // 4
  Sound { 13,  4, A5, 22 },  // 5
  Sound { 15,  5, 12, 13 },  // 6
  Sound { 19,  6, A2, 10 },  // 7
  Sound { 20,  7, 13, 13 },  // 8
  Sound { 24,  9, 10, 13 },  // 9
  Sound { 25,  8, 11, 20 }   // 10
};

enum State {
  WAITING,        
  PLAYING_SOUND
} state = WAITING;

Sound playingSound;
long stopPlayingTime = millis();

void setup() {
  pinMode(0, OUTPUT);
  if (SERIAL) {
    Serial.begin(9600);
    Serial.println("");  
  }
  for (int i = 0; i < SOUNDS_COUNT; i++) {
    if (SERIAL) {
      Serial.print("Setup square: ");
      Serial.print(sounds[i].square);
      Serial.print(", out: ");
      Serial.print(sounds[i].out);
      Serial.print(", in: ");
      Serial.print(sounds[i].in);
      Serial.print(", len: ");
      Serial.print(sounds[i].len);
      Serial.println();
    }
    
    pinMode(sounds[i].in, INPUT_PULLUP);
    pinMode(sounds[i].out, OUTPUT);
    digitalWrite(sounds[i].out, HIGH);
    sounds[i].previousInState = HIGH;
    sounds[i].newTrigger = false;
  }
}

void loop() {
  updateSquares();
  Sound s;
  switch(state) {
    case WAITING:
      s = newTrigger();
      if (s.square != 0) {
        if (SERIAL) {
          Serial.print("playing square ");
          Serial.println(s.square);
        }
        digitalWrite(s.out, LOW);
        stopPlayingTime = millis() + 1000 * s.len;
        state = PLAYING_SOUND;
        playingSound = s;
      }
      break;
    case PLAYING_SOUND:
      if (digitalRead(playingSound.in) == HIGH || millis() > stopPlayingTime) {
        writeAllHigh();
        state = WAITING;
        if (Serial) {
          Serial.println("Stoping sound.");
        }
      }
      break;
  }
  delay(500);
}


void writeAllHigh() {
  for (int i = 0; i < SOUNDS_COUNT; i++) {
    digitalWrite(sounds[i].out, HIGH);
  }
}

Sound newTrigger() {
  for (int i = 0; i < SOUNDS_COUNT; i++) {
    if (sounds[i].newTrigger) {
      return sounds[i];
    }
  }
  return Sound {};
}

void updateSquares() {
  for (int i = 0; i < SOUNDS_COUNT; i++) {
    boolean newIn = digitalRead(sounds[i].in);
    sounds[i].newTrigger = newIn == LOW && sounds[i].previousInState == HIGH;
    if (SERIAL && sounds[i].newTrigger) {
      Serial.print("New trigger from ");
      Serial.println(sounds[i].square);
    }
    if (sounds[i].square == 1 && false) {
      Serial.println("=============");
      Serial.println(newIn);
      Serial.println(sounds[i].previousInState);
    }
    sounds[i].previousInState = newIn;
  }
}

