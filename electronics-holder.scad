wt = 2;    // wall thickness
bt = 2;     // base thickness

$fa = 1;
$fs = 0.5;

module base_bolt_hole() {
    difference() {
        cylinder(d = 10, h = 6);
        cylinder(d = 3.5, h = 6);
        translate([0, 0, 4])
        cylinder(d = 3.5+4+0.3, h = 6);
        
    }
}

module base_bolt_holder_neg() {
    cylinder(d = 6.3, h = 2.5, , $fa = 60);
}
//
//screw_hole();


module base_bolt_test() {
    difference() {
        union() {
            translate([0, 0, 1])
            cube([15, 15, 2], center = true);
            base_bolt_hole();
        }
        base_bolt_holder_neg();
    }
}

module top_bolt_holder() {
    difference() {
        union() {
            cylinder(d = 10, h = 4);
            cylinder(d = 3.5+4, h = 6);
        }
        cylinder(d = 3.5, h = 6);
    }
}

module top_bolt_holder_neg() {
    cylinder(d = 6, h = 2);
}

module top_bolt_test() {
    difference() {
        union() {
            translate([0, 0, 1])
            cube([15, 15, 2], center = true);
            top_bolt_holder();
        }
        top_bolt_holder_neg();
    }
}

//base_bolt_test();
//top_bolt_test();

module base() {
    difference() {
        union() {
            cube([72, 60, 8]);
            translate([12, 60, 0]) cylinder(d = 24, h = 8);
            translate([12, 0, 0]) cube([60, 72, 8]);
        }
        translate([2, -2, 2]) {
            cube([72, 62, 8]);
            translate([10, 62, 0]) cylinder(d = 20, h = 8);
            translate([10, 0, 0]) cube([62, 72, 8]);
        }
        
    }    
}

module base2() {
    difference() {
        union() {
            cube([72, 60, 3]);
            translate([12, 60, 0]) cylinder(d = 24, h = 3);
            translate([12, 0, 0]) cube([60, 72, 3]);
        }
        translate([2, -2, 2]) {
            cube([72, 62, 8]);
            translate([10, 62, 0]) cylinder(d = 20, h = 8);
            translate([10, 0, 0]) cube([62, 72, 8]);
        }
        
    }    
}

module audio_hole() {
    translate([0, 0, 5.5/2])
    rotate([0, -90, 0]) {
        cylinder(d = 5.5, h = 4);
        translate([0, -5.5/2, 0])
        cube([10, 5.5, 4]);
    }
    
}

module base_total() {
    difference() {
        union() {
            base();
            translate([10, 6, 0]) base_bolt_hole();
            translate([66, 6, 0]) base_bolt_hole();
            translate([66, 62, 0]) base_bolt_hole();
            translate([14, 65, 0]) base_bolt_hole();
            
            translate([0, 3, 0]) {
                // Audio back holder
                translate([2, 0, 0])
                translate([47.5, 45-23/2, 0]) {
                    
                    cube([6,6,6]);
                    translate([0, 17, 0])
                    cube([6,6,6]);
                }
                
                translate([2, 0, 0])
                translate([43.5, 45-26, 0])
                cube([6,6,6]);
            }
            
        }
        translate([10, 6, 0]) base_bolt_holder_neg();
        translate([66, 6, 0]) base_bolt_holder_neg();
        translate([66, 62, 0]) base_bolt_holder_neg();
        translate([14, 65, 0]) base_bolt_holder_neg();
        
        translate([0, 3, 0]) {
            
            translate([3, 45, 3])  audio_hole();
            
            translate([0, 45-26, 6])
            cube([5, 10,6], center = true);
        }
    }
}
module top_total() {
    mirror([0, 0, 1])
    difference() {
        union() {
            base2();
            
            /*
            cube([72, 60, 2]);
            translate([12, 60, 0]) cylinder(d = 24, h = 2);
            translate([12, 0, 0]) cube([60, 72, 2]);
            */
            
            
            translate([10, 6, 1]) top_bolt_holder();
            translate([66, 6, 1]) top_bolt_holder();
            translate([66, 62, 1]) top_bolt_holder();
            translate([14, 65, 1]) top_bolt_holder();
        }
        
        translate([10, 6, 0]) top_bolt_holder_neg();
        translate([66, 6, 0]) top_bolt_holder_neg();
        translate([66, 62, 0]) top_bolt_holder_neg();
        translate([14, 65, 0]) top_bolt_holder_neg();
    }
}
translate([0, 0, 20]) top_total();

//base_total();
